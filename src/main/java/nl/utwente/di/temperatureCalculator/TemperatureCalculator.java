package nl.utwente.di.temperatureCalculator;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class TemperatureCalculator extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TemperatureConverter temperatureConverter;
	
    public void init() throws ServletException {
    	temperatureConverter = new TemperatureConverter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temperature Calculation";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Temperature in degrees Celsius: " +
                   request.getParameter("Temperature in celsius") + "\n" +
                "  <P>Temperature in degrees Fahrenheit: " +
                   temperatureConverter.getFahrenheitTemp(Double.parseDouble(request.getParameter("Temperature in celsius"))) +
                "</BODY></HTML>");
  }
}
