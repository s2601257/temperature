package nl.utwente.di.temperatureCalculator;

public class TemperatureConverter {
    public TemperatureConverter() {
    }
    public double getFahrenheitTemp(double celTemp) {
        double fahrTemp = (celTemp*1.8) + 32;
        return fahrTemp;
    }
}
