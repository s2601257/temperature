package nl.utwente.di.temperatureCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/*** Tests the Quoter */
public class TestTemperatureConverter {
    @Test
    public void test() throws Exception {
        TemperatureConverter temperatureConverter = new TemperatureConverter();
        double price = temperatureConverter.getFahrenheitTemp(24.5);
        Assertions.assertEquals(76.1,price,0.0, "Temperature in Fahrenheit");
    }
}

